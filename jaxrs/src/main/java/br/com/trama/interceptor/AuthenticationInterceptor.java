package br.com.trama.interceptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import br.com.trama.model.Credential;
import br.com.trama.rest.SecurityManager;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;


public class AuthenticationInterceptor implements ContainerRequestFilter{

	private Logger logger = Logger.getLogger(AuthenticationInterceptor.class);

	static{
		BasicConfigurator.configure();
	}
	
	@Override
	public ContainerRequest filter(ContainerRequest containerRequest) {

		String path = containerRequest.getAbsolutePath().getPath();
		
		logger.info("path: "+path);
		
		if(!ignoreRequest(path)){

			String token = containerRequest.getQueryParameters().getFirst("token");
			
			logger.info("token: "+token);
			
			Credential credential = SecurityManager.getInstance().get(token);
			
			if(credential == null){
				logger.info("credential not found");
				throw new WebApplicationException(Response.Status.UNAUTHORIZED);
			}
			
			if(!containerRequest.isUserInRole(credential.getRole())){
				logger.info("role not found");
				throw new WebApplicationException(Response.Status.UNAUTHORIZED);
			}
		}

		return containerRequest;
	}
	
	private boolean ignoreRequest(String requestURI) {
		return requestURI.contains("index.html") || requestURI.contains("/login");
	}
}
