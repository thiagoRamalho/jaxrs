package br.com.trama.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import br.com.trama.model.Credential;
import br.com.trama.model.Result;
import br.com.trama.model.UserDAO;
import br.com.trama.util.Constants;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.util.Base64;

@Path("/restricted")
public class PainelRest {

	private Logger logger = Logger.getLogger(PainelRest.class);
	
	@InjectParam
	private UserDAO userDAO;
	
	static{
		BasicConfigurator.configure();
	}

	@Path("/login")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@FormParam("username") String username, @FormParam("password") String password){

		logger.info("username: "+username + " password: "+password);
		
		Result result = new Result();
		Status status = null;
		
		Credential credential = this.userDAO.findByUsername(username);
		
		if(!isValid(username) || !isValid(password) || credential == null){
			status = Response.Status.UNAUTHORIZED;
			result.setMsg(Constants.MSG_UNAUTHORIZED);
			result.setParam("");
		} else {
			status = Response.Status.ACCEPTED;
			result.setMsg(Constants.MSG_ACCEPT);
			String token = Base64.decode(username+":"+password).toString().replaceAll("[^0-9a-zA-Z]", "0");
			result.setParam(token);
			credential.setToken(token);
			
			result.setUsers(new Credential[]{credential});
			
			SecurityManager.getInstance().put(credential);
		}
		
		log("/login", result);
		
		return createResponse(status, result);
	}
	
	@RolesAllowed(Constants.ADMIN)
	@Path("/listadmin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listadmin(@QueryParam("token") String token){
		
		logger.info("token: "+token);
		
		Result result = new Result();
		Status status = Response.Status.UNAUTHORIZED;
		String msg = Constants.MSG_UNAUTHORIZED;
		
		Credential credential = SecurityManager.getInstance().get(token);
		
		if(credential != null && credential.getRole().equals(Constants.ADMIN)){
			status = Response.Status.OK;
			msg = Constants.MSG_OK;
			result.setUsers(this.userDAO.findAll());
		}
		
		result.setMsg(msg);
		result.setParam(token);
		
		log("/listadmin", result);
		
		return createResponse(status, result);   
	}
	
	@RolesAllowed(Constants.COMUM)
	@Path("/listcomum")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listcomum(@QueryParam("token") String token){
		
		logger.info("token: "+token);
		
		Result result = new Result();
		Status status = Response.Status.UNAUTHORIZED;
		String msg = Constants.MSG_UNAUTHORIZED;
		
		if(SecurityManager.getInstance().contains(token)){
			status = Response.Status.OK;
			msg = Constants.MSG_OK;
			
			result.setUsers(this.userDAO.findComum());
		}
		
		result.setMsg(msg);
		result.setParam(token);
		
		log("/listcomum", result);
		
		Response createResponse = createResponse(status, result);
		
		return createResponse;
	}


	private void log(String resource, Result result) {
		logger.info("resource: "+ resource + " " +result.toString());
	}

	private Response createResponse(Status status, Result result) {
		return Response.status(status).entity(result).build();
	}

	private boolean isValid(String value) {
		return value != null && !value.isEmpty();
	}
}
