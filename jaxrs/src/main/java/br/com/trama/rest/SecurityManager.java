package br.com.trama.rest;

import java.util.HashMap;

import br.com.trama.model.Credential;

import com.sun.jersey.spi.resource.Singleton;

@Singleton
public class SecurityManager {

	private static SecurityManager instance;
	private HashMap<String, Credential> map;
	
	private SecurityManager(){
		this.map = new HashMap<String, Credential>();
	}
	
	public static SecurityManager getInstance() {
		
		if(instance == null){
			instance = new SecurityManager();
		}
		
		return instance;
	}
	
	public void put(Credential credential) {
		this.map.put(credential.getToken(), credential);
	}
	
	public void remove(String token){
		this.map.remove(token);
	}

	public boolean contains(String token) {
		return this.map.containsKey(token);
	}

	public Credential get(String token) {
		return this.map.get(token);
	}
}
