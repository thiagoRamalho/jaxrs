package br.com.trama.model;

import java.io.Serializable;
import java.util.Arrays;


public class Result implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String msg;
	String param;
	Credential[] users;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	
	public void setUsers(Credential[] users) {
		this.users = users;
	}
	
	public Credential[] getUsers() {
		return users;
	}
	@Override
	public String toString() {
		return "Result [msg=" + msg + ", param=" + param + ", users="
				+ Arrays.toString(users) + "]";
	}
}
