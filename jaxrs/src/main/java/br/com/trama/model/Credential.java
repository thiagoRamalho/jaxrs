package br.com.trama.model;

public class Credential {

	String token = "";
	String role = "";
	String username = "";

	public Credential(String username, String role) {
		super();
		this.username = username;
		this.role = role;
	}
	
	public String getRole() {
		return role;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getUsername() {
		return username;
	}
	
	@Override
	protected Credential clone() {
		return new Credential(username, role);
	}

	@Override
	public String toString() {
		return "Credential [token=" + token + ", role=" + role + ", username="
				+ username + "]";
	}
}
