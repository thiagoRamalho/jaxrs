package br.com.trama.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.trama.util.Constants;

public class UserDAO {

	private Map<String, Credential> users;

	public UserDAO() {
		super();
		this.users = new HashMap<String, Credential>();
		this.users.put("admin", new Credential("admin", Constants.ADMIN));
		this.users.put("comum", new Credential("comum", Constants.COMUM));
	}

	public Credential findByUsername(String username) {

		Credential credential = this.users.get(username);

		if(credential != null){
			credential =  credential.clone();
		}

		return credential;
	}

	public Credential[] findAll() {

		return this.users.values().toArray(new Credential[this.users.size()]);
	}

	public Credential[] findComum() {

		List<Credential> list = new ArrayList<Credential>();

		for (Credential credential : this.users.values()) {
			if(Constants.COMUM.equals(credential.getRole())){
				list.add(credential);
			}
		}

		return list.toArray(new Credential[list.size()]);
	}
}
