package br.com.trama.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class AuthenticationFilter implements Filter{

	private Logger logger = Logger.getLogger(AuthenticationFilter.class);

	static{
		BasicConfigurator.configure();
	}
	
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("init");
	}

	public void destroy() {
		logger.info("destroy");
	}

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		
		logger.info("doFilter");

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String token = req.getParameter("token");
		
		if(!ignoreRequest(req.getRequestURI()) && (token == null || token.isEmpty())){
			logger.info("redirect");
			res.sendRedirect("../index.html");
		} else {
			logger.info("doFilter chain");
			chain.doFilter(req, res);
			
			if(res.getStatus() == HttpServletResponse.SC_UNAUTHORIZED){
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "HttpServletResponse.SC_UNAUTHORIZED");
			}
		}
	}

	private boolean ignoreRequest(String requestURI) {
		return requestURI.contains("index.html") || requestURI.contains("/login");
	}
}
